import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  public apiUrl = 'https://desafio-it-server.herokuapp.com';

  constructor(private httpClient: HttpClient) {}

  public get(endpoint: string): Observable<any> {
    return this.httpClient.get(this.apiUrl + endpoint).pipe(take(1));
  }

  public getPromise(endpoint: string): Promise<any> {
    return this.httpClient.get(this.apiUrl + endpoint).toPromise();
  }
}
