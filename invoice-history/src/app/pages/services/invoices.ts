export class Invoices {
  public id: string;
  public origem: string;
  public categoria: string;
  public valor: number;
  public mes_lancamento: string;
}
