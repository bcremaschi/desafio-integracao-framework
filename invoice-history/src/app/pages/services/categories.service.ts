import { Injectable } from '@angular/core';

import { HttpService } from './../../core/services/http/http.service';
import { Categories } from './categories';

@Injectable({
  providedIn: 'root',
})
export class CategoriesService {
  constructor(private httpService: HttpService) {}

  get(category: string): Promise<Categories> {
    return this.httpService.getPromise('/categorias/' + category);
  }
}
