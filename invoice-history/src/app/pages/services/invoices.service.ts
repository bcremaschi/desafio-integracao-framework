import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpService } from './../../core/services/http/http.service';
import { Invoices } from './invoices';

@Injectable({
  providedIn: 'root',
})
export class InvoicesService {
  constructor(private httpService: HttpService) {}

  get(): Observable<Array<Invoices>> {
    return this.httpService.get('/lancamentos');
  }
}
