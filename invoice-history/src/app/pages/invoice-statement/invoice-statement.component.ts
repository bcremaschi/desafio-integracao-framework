import { Component, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

import { CategoriesService } from './../services/categories.service';
import { InvoicesService } from './../services/invoices.service';
import { Invoices } from '../services/invoices';

const MONTH_NAMES = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maior',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro',
];

@Component({
  selector: 'app-invoice-statement',
  templateUrl: './invoice-statement.component.html',
  styleUrls: ['./invoice-statement.component.scss'],
})
export class InvoiceStatementComponent implements OnInit {
  public historyCollumns: Array<string>;
  public consolidatedCollumns: Array<string>;
  public historyItems = [];
  public consolidatedItems = [];
  public categoryName = '';

  constructor(
    private invoiceService: InvoicesService,
    private categoriesService: CategoriesService,
    private currencyPipe: CurrencyPipe
  ) {}

  ngOnInit() {
    this.historyCollumns = ['Origem', 'Categoria', 'Valor gasto', 'Mês'];
    this.consolidatedCollumns = ['Mês', 'Total gasto'];
    this.getInvoiceHistory();
  }

  private getInvoiceHistory(): void {
    this.invoiceService.get().subscribe(
      (response: Array<Invoices>) => {
        this.setHistoryItems(response);
        this.setConsolidatedItems(response);
      },
      (error) => console.log(error)
    );
  }

  async getCateroryName(category: string) {
    let categoryName: string;
    await this.categoriesService.get(category).then((res) => {
      categoryName = res.nome;
    });
    return categoryName;
  }

  private setHistoryItems(invoices: Array<Invoices>): void {
    invoices.map(async (invoice) => {
      this.historyItems.push([
        invoice.origem,
        await this.getCateroryName(invoice.categoria),
        this.currencyPipe.transform(invoice.valor, 'BRL', 'R$ ', '1.2-2'),
        MONTH_NAMES[invoice.mes_lancamento],
      ]);
    });
  }

  private setConsolidatedItems(invoices: Array<Invoices>): void {
    invoices.map(async (invoice) => {
      const month = invoice.mes_lancamento;
      const foundMonth = this.consolidatedItems.find(
        (item) => item.mes_lancamento === month
      );

      if (foundMonth) {
        this.consolidatedItems[foundMonth] += invoice.valor;
      } else {
        this.consolidatedItems.push([
          MONTH_NAMES[month],
          this.currencyPipe.transform(invoice.valor, 'BRL', 'R$ ', '1.2-2'),
        ]);
      }
    });
  }
}
