import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';

import { SharedModule } from './../shared/shared.module';
import { CoreModule } from './../core/core.module';
import { InvoiceStatementComponent } from './invoice-statement/invoice-statement.component';

@NgModule({
  declarations: [InvoiceStatementComponent],
  imports: [CommonModule, SharedModule, CoreModule],
  exports: [InvoiceStatementComponent],
  providers: [CurrencyPipe],
})
export class PagesModule {}
